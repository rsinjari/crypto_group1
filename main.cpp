#include <iostream>
#include <memory>

#include "include/Crypto.h"
#include "include/CryptoImpl.h"
#include "include/CryptoChain.h"
#include "include/Sinjari.h"
#include "include/rechka.h"



using namespace std;

int main() {

    CryptoChain cc;

    auto_ptr<Sinjari> c1(new Sinjari);
    auto_ptr<Crypto> c2(new CryptoImpl);
    auto_ptr<rechka> c3(new rechka);

    cc.add(c1.get());
    cc.add(c2.get());
    cc.add(c3.get());

    string input = "Hello, World!";
    string encoded = cc.encode(input);
    string decoded = cc.decode(encoded);

    cout << input << " -> " << encoded << " -> " << decoded;
}
