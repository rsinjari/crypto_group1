#ifndef SINJARI_H
#define SINJARI_H


#include "Crypto.h"

class Sinjari : public Crypto
{
    public:
        Sinjari();
        virtual ~Sinjari();

        std::string encode(std::string s);
        std::string decode(std::string s);

     protected:
    private:

};

#endif // RSINJARI_H

