#ifndef RECHKA_H
#define RECHKA_H

#include "Crypto.h"

class rechka : public Crypto
{
    public:
        rechka();
        virtual ~rechka();

        std::string encode(std::string s);
        std::string decode(std::string s);

    protected:
    private:
};

#endif // RECHKA_H
